class GameArtifactItem {
  String itemGrade;
  String itemName;
  String imageName;
  double woodBoxPercent;
  double goodBoxPercent;

  GameArtifactItem(this.itemGrade, this.itemName, this.imageName, this.woodBoxPercent, this.goodBoxPercent);
}
