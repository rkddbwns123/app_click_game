import 'package:app_click_game/components/component_collection_item.dart';
import 'package:app_click_game/config/config_Init_data.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PageCollection extends StatefulWidget {
  const PageCollection({Key? key}) : super(key: key);

  @override
  State<PageCollection> createState() => _PageCollectionState();
}

class _PageCollectionState extends State<PageCollection> {
  List<String> _useArtifacts = [];

  void _getUseArtifacts() async{
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      _useArtifacts = prefs.getStringList('useArtifacts') == null ? [] : prefs.getStringList('useArtifacts')!;
    });
  }

  @override
  void initState() {
    super.initState();

    _getUseArtifacts();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _useArtifacts.length == 0 ? Container() : ListView.builder(
        itemCount: configArtifacts.length,
        itemBuilder: (BuildContext ctx, int idx) {
          return ComponentCollectionItem(
              imageName: configArtifacts[idx].imageName,
              itemGrade: configArtifacts[idx].itemGrade,
              itemName: configArtifacts[idx].itemName,
              itemCount: int.parse(_useArtifacts[idx])
          );
        },
      )

    );
  }
}