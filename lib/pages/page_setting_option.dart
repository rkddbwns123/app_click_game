import 'package:app_click_game/config/config_Init_data.dart';
import 'package:app_click_game/model/game_artifact_item.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PageSettingOption extends StatelessWidget {
  const PageSettingOption({Key? key}) : super(key: key);

  void _resetData() async{
    final prefs = await SharedPreferences.getInstance();
    prefs.clear();
    prefs.setInt('money', 0);

    List<String> result = [];

    for(GameArtifactItem item in configArtifacts) {
      result.add('0');
    }
    
    prefs.setStringList('useArtifacts', result);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: ElevatedButton(
          onPressed: () {
            _asyncConfirmDialog(context);
          },
          child: Text('게임 초기화'),
        ),
      )
    );
  }
  Future<void> _asyncConfirmDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('게임 초기화'),
          content: Text('초기화 하시겠습니까?'),
          actions: <Widget>[
            TextButton(child: Text('취소'), onPressed: () {
              Navigator.of(context).pop();
            },
            ),
            TextButton(child: Text('확인'), onPressed: () {
              _resetData();
              Navigator.of(context).pop();
            })
          ],
        );
      }
    );
  }
}
