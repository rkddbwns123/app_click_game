import 'package:app_click_game/config/config_Init_data.dart';
import 'package:app_click_game/model/game_artifact_item.dart';
import 'package:app_click_game/pages/page_collection.dart';
import 'package:app_click_game/pages/page_in_game.dart';
import 'package:app_click_game/pages/page_setting_option.dart';
import 'package:flutter/material.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  int _currentIndex = 0;

  final List<BottomNavyBarItem> _navItems = [
    BottomNavyBarItem(
      icon: const Icon(Icons.play_circle_filled),
      title: const Text('플레이'),
      activeColor: Colors.red,
      textAlign: TextAlign.center,
    ),
    BottomNavyBarItem(
      icon: const Icon(Icons.photo),
      title: const Text('컬렉션'),
      activeColor: Colors.purpleAccent,
      textAlign: TextAlign.center,
    ),
    BottomNavyBarItem(
      icon: const Icon(Icons.settings),
      title: const Text('옵션'),
      activeColor: Colors.blue,
      textAlign: TextAlign.center,
    ),
  ];

  final List<Widget> _widgetPages = [
    PageInGame(),
    PageCollection(),
    PageSettingOption(),
  ];

  void _onItemTap(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  void initState() {
    super.initState();

    checkUserArtifacts();
  }

  void checkUserArtifacts() async {
    final prefs = await SharedPreferences.getInstance();
    List<String>? checkData = prefs.getStringList('userArtifacts');

    bool checkIsEmpty = checkData?.isEmpty ?? true;

    List<String> result = [];
    if(checkIsEmpty) {
      for(GameArtifactItem item in configArtifacts) {
        result.add('0');
      }

      prefs.setStringList('userArtifacts', result);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: _widgetPages.elementAt(_currentIndex),
      ),
      bottomNavigationBar: BottomNavyBar(
        selectedIndex: _currentIndex,
        showElevation: true,
        itemCornerRadius: 24,
        curve: Curves.easeIn,
        onItemSelected: _onItemTap,
        items: _navItems,
      ),
    );
  }
}