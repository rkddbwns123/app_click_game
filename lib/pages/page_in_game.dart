import 'dart:math';

import 'package:app_click_game/config/config_Init_data.dart';
import 'package:app_click_game/model/game_artifact_item.dart';
import 'package:app_click_game/model/open_box_percent_item.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';


class PageInGame extends StatefulWidget {
  const PageInGame({Key? key}) : super(key: key);

  @override
  State<PageInGame> createState() => _PageInGameState();
}

class _PageInGameState extends State<PageInGame> {
  int _money = 0;
  List<String> _useArtifacts = [];


  @override
  void initState() {
    super.initState();
    _initMoney();
    _getUseArtifacts();
  }

  void _initMoney() async {
    final prefs = await SharedPreferences.getInstance();
    int savedMoney = prefs.getInt('money') ?? 0;
    setState(() {
      _money = savedMoney;
    });
  }

  void _saveMoney() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setInt('money', _money);
  }


  void _gatherMoney() {
    setState(() {
      _money += 1000;
    });
  }

  void _getUseArtifacts() async{
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      _useArtifacts = prefs.getStringList('useArtifacts') == null ? [] : prefs.getStringList('useArtifacts')!;
    });
  }

  void _getOpenBoxResult(bool isWoodBox) {
    int openBoxPay = isWoodBox ? 5000 : 15000;
    bool isEnoughMoney = _isEnoughMoneyCheck(openBoxPay);

    if(isEnoughMoney) {
      setState(() {
        _money -= openBoxPay;
      });

      int artifactResultId = _openBoxResult(isWoodBox);

      bool isNewArtifact = false;

      int openBoxArtifactCount = int.parse(_useArtifacts[artifactResultId]);
      if(openBoxArtifactCount == 0) isNewArtifact = true;

      _plusUseArtifact(artifactResultId, openBoxArtifactCount);
      _getUseArtifacts();
      _dialogArtifact(artifactResultId, isNewArtifact);
    }
  }

  void _plusUseArtifact(int index, int oldCount) async {
    List<String> useArtifactTemp = _useArtifacts;
    _useArtifacts[index] = (oldCount + 1).toString();

    final prefs = await SharedPreferences.getInstance();
    prefs.setStringList('useArtifacts', useArtifactTemp);
  }

  bool _isEnoughMoneyCheck(int openBoxPay) {
    bool result = false;

    if(_money >= openBoxPay) result = true;
    return result;
  }

  int _openBoxResult(bool isWoodBox) {
    List<GameArtifactItem> gameItemStats = configArtifacts;

    List<OpenBoxPercentItem> percentBar = [];

    double oldPercent = 0;
    int index = 0;
    for(GameArtifactItem gameArtifactItem in gameItemStats) {
      OpenBoxPercentItem addItem = OpenBoxPercentItem(
          index,
          oldPercent,
          isWoodBox ? oldPercent + gameArtifactItem.woodBoxPercent : oldPercent + gameArtifactItem.goodBoxPercent);
      percentBar.add(addItem);

      if(isWoodBox) {
        oldPercent += gameArtifactItem.woodBoxPercent;
      } else {
        oldPercent += gameArtifactItem.goodBoxPercent;
      }
      index++;

    }
    double result = Random().nextDouble() * 100;

    int resultId = 0;
    for(OpenBoxPercentItem item in percentBar) {
      if(result >= item.percentMin && result <= item.percentMax) {
        resultId = item.id;
        break;
      }
    }

    return resultId;
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Stack(
            children: [
              SizedBox(
                  width: MediaQuery.of(context).size.width,
                  height: 300,
                  child: Container(color: Colors.lightBlueAccent[100])),
              Positioned(
                  top: 60,
                  left: MediaQuery.of(context).size.width / 2 - 90,
                  child: SizedBox(
                      width: 180,
                      height: 180,
                      child: ElevatedButton(
                        onPressed: _gatherMoney,
                        child: Image.asset('assets/moneybag.png',
                            fit: BoxFit.fill),
                      ))),
            ],
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width,
            height: 40,
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(
                      color: Colors.grey, width: 2, style: BorderStyle.solid)),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text('보유 금액   ', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
                  Container(
                    width: 120,
                    height: 30,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                            color: Colors.black,
                            width: 2,
                            style: BorderStyle.solid)),
                    child: Text('$_money G', style: const TextStyle(fontSize: 22, fontWeight: FontWeight.bold), textAlign: TextAlign.center)
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(
            height: 30,
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: 350,
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.black,
                width: 2,
                style: BorderStyle.solid
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width / 2 - 2,
                  height: MediaQuery.of(context).size.height,
                  decoration: const BoxDecoration(
                      border: Border(
                        right: BorderSide(
                            color: Colors.grey,
                            width: 2,
                            style: BorderStyle.solid
                        ),
                      )
                  ),
                  child: Column(
                    children: [
                      SizedBox(
                        width: 180,
                        height: 180,
                        child: GestureDetector(
                          onTap: () {
                            _getOpenBoxResult(true);
                          },
                          child: Image.asset(
                            'assets/woodbox.png',
                            fit: BoxFit.fill,
                          ),
                        )
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 130,
                        decoration: const BoxDecoration(
                          border: Border(
                            top: BorderSide(
                                color: Colors.grey,
                                width: 2,
                                style: BorderStyle.solid
                            ),
                          ),
                        ),
                        child: const Text('\n 나무상자 열기 \n\n 5000 G', style: TextStyle(
                            color: Colors.black,
                            fontSize: 20,
                            fontWeight: FontWeight.bold
                        ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width / 2 - 2,
                  height: MediaQuery.of(context).size.height,
                  decoration: const BoxDecoration(
                      border: Border(
                        right: BorderSide(
                            color: Colors.grey,
                            width: 2,
                            style: BorderStyle.solid
                        ),
                      )
                  ),
                  child: Column(
                    children: [
                      SizedBox(
                          width: 180,
                          height: 180,
                          child: GestureDetector(
                            onTap: () {
                              _getOpenBoxResult(false);
                            },
                            child: Image.asset(
                              'assets/goodbox.png',
                              fit: BoxFit.fill,
                            ),
                          )
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 130,
                        decoration: const BoxDecoration(
                          border: Border(
                            top: BorderSide(
                                color: Colors.grey,
                                width: 2,
                                style: BorderStyle.solid
                            ),
                          ),
                        ),
                        child: const Text('\n 좋아 보이는 상자 열기 \n\n 15000 G', style: TextStyle(
                            color: Colors.black,
                            fontSize: 20,
                            fontWeight: FontWeight.bold
                        ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );

  }

  Future<void> _dialogArtifact(int artifactId, bool isNew) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('상자 개봉 !'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                child: isNew ? Text('새로운 유물을 발견했습니다 !') : Text(''),
              ),
              SizedBox(
                width: 100,
                height: 100,
                child: Image.asset('${configArtifacts[artifactId].imageName}'),
              ),
              Text('${configArtifacts[artifactId].itemGrade}'),
              Text('${configArtifacts[artifactId].itemName}')
            ],
          ),
          actions: [
            TextButton(
              child: Text('확인'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        );
      }
    );
  }

  @override
  void dispose() {
    super.dispose();
    _saveMoney();
  }
}
