import '../model/game_artifact_item.dart';

final List<GameArtifactItem> configArtifacts = [
  GameArtifactItem('COMMON', '뼈다귀', 'assets/bone.png', 10, 5),
  GameArtifactItem('COMMON', '정체 모를 가루', 'assets/sugar.png', 10, 5),
  GameArtifactItem('COMMON', '발광석 가루', 'assets/light.png', 10, 5),
  GameArtifactItem('COMMON', '레드스톤 가루', 'assets/redstone.png', 10, 5),
  GameArtifactItem('COMMON', '화약 가루', 'assets/gunpowder.png', 10, 5),
  GameArtifactItem('UNCOMMON', '석탄 가루', 'assets/coal.png', 6, 8),
  GameArtifactItem('UNCOMMON', '숯', 'assets/charcoal.png', 6, 8),
  GameArtifactItem('UNCOMMON', '구리', 'assets/copper.png', 6, 8),
  GameArtifactItem('UNCOMMON', '부싯돌', 'assets/flint.png', 6, 8),
  GameArtifactItem('UNCOMMON', '벽돌', 'assets/brick.png', 6, 8),
  GameArtifactItem('SPECIAL', '수정', 'assets/crystal.png', 3, 5),
  GameArtifactItem('SPECIAL', '석영', 'assets/quartz.png', 3, 5),
  GameArtifactItem('SPECIAL', '은', 'assets/silver.png', 3, 5),
  GameArtifactItem('SPECIAL', '금', 'assets/gold.png', 3, 5),
  GameArtifactItem('SPECIAL', '루비', 'assets/ruby.png', 3, 5),
  GameArtifactItem('UNIQUE', '청금석', 'assets/lazurite.png', 1.2, 2.25),
  GameArtifactItem('UNIQUE', '자수정', 'assets/amethyst.png', 1.2, 2.25),
  GameArtifactItem('UNIQUE', '에메랄드', 'assets/emerald.png', 1.2, 2.25),
  GameArtifactItem('UNIQUE', '다이아몬드', 'assets/diamond.png', 1.2, 2.25),
  GameArtifactItem('LEGENDARY', '전설의 광석', 'assets/legendary.png', 0.2, 1),

];