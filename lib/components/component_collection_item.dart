import 'dart:ui';

import 'package:flutter/material.dart';

class ComponentCollectionItem extends StatelessWidget {
  const ComponentCollectionItem({super.key,
    required this.imageName,
    required this.itemGrade,
    required this. itemName,
    required this. itemCount});

  final String imageName;
  final String itemGrade;
  final String itemName;
  final int itemCount;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 100,
      decoration: BoxDecoration(
          border: Border.all(
              color: Colors.black
          )
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const SizedBox(
            width: 10,
          ),
          Container(
            width: 80,
            height: 80,
            decoration: BoxDecoration(
                border: Border.all(
                  style: BorderStyle.solid,
                  width: 2,
                  color: Colors.black38,
                )
            ),
            child: _getImage()
          ),
          const SizedBox(
            width: 25,
          ),
          Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 90,
                height: 25,
                padding: const EdgeInsets.all(1),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                        style: BorderStyle.solid,
                        width: 2,
                        color: Colors.black38
                    )
                ),
                child: itemCount == 0 ? Text('-') : _getGradeBadge(),
              ),
              const SizedBox(
                height: 10,
              ),
              Container(
                width: 130,
                height: 45,
                padding: const EdgeInsets.all(9),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                        style: BorderStyle.solid,
                        width: 2,
                        color: Colors.black38
                    )
                ),
                child: itemCount == 0 ? Text('-') : Text(itemName,
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: Colors.black),
                  textAlign: TextAlign.center,),
              )
            ],
          ),
          const SizedBox(
            width: 30,
          ),
          Container(
              width: 100,
              height: 80,
              decoration: BoxDecoration(
                  border: Border.all(
                      color: Colors.black38
                  )
              ),
              child: Column(
                children: [
                  const Text('보유 수량',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Colors.black),
                    textAlign: TextAlign.center,),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 50,
                    padding: const EdgeInsets.all(12),
                    decoration: const BoxDecoration(
                        border: Border(
                          top: BorderSide(
                              color: Colors.black38
                          ),
                        )
                    ),
                    child: itemCount == 0 ? Text('-') : Text(itemCount.toString(),
                      style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Colors.black),
                      textAlign: TextAlign.center,),
                  ),
                ],
              )
          ),
          const SizedBox(
            width: 10,
          )
        ],
      ),
    );
  }

  Widget _getGradeBadge() {
    switch(itemGrade) {
      case 'UNCOMMON':
        return Text('안흔함', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.green),
            textAlign: TextAlign.center);
      case 'SPECIAL':
        return Text('특별함', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.amber),
            textAlign: TextAlign.center);
      case 'UNIQUE':
        return Text('희귀함', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.pinkAccent),
            textAlign: TextAlign.center);
      case 'LEGENDARY':
        return Text('전설적인', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red),
            textAlign: TextAlign.center);
      default:
        return Text('흔함', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.grey),
    textAlign: TextAlign.center);
    }
  }
  Widget _getImage() {
    if(itemCount == 0) {
      return ImageFiltered(
        imageFilter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
        child: Image.asset(
          imageName, fit: BoxFit.fill,
        ),
      );
    } else {
      return Image.asset(imageName, fit: BoxFit.fill,);
    }
  }
}
